# Cloud Firewall IP Updater 

A Python application that updates a cloud provider firewall/security group based off of your current IP address.

## Prerequisites
* Python 3.8
* [pipenv](https://pipenv.pypa.io/en/latest/) is used for environment management. 
* AWS -  Profile configured with a default profile specified has necessary permissions to update an EC2 Security Group (you can have this setup without having AWSCLI installed see [HERE](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)
* Digital Ocean  - An API Token with proper access to Firewall API. Instructions [HERE](https://www.digitalocean.com/docs/apis-clis/api/create-personal-access-token/) for how to create
* Hetzner Cloud - An API Token with read and write access to Firewalls. Create one in the [Hetzner Cloud Console](https://console.hetzner.cloud/) under Security > API Tokens.
* 1Password CLI (optional) - For secure storage of API tokens and firewall IDs

## How do I get set up? 

* Run `pipenv install` to install the python requirements listed in Pipefile/Pipfile.lock in a new virtual environment. Make sure to install pipenv from above link before this.

### 1Password Setup (Optional)
If you want to use 1Password to store your credentials:

1. Install the 1Password CLI
2. Create an item named "Hetzner Cloud API Token" with the following fields:
   - token: Your Hetzner Cloud API token
   - firewallrule: Your Hetzner Cloud firewall ID

## Running the script

Before you can run the program you will need to activate/enter your python virtual environment using `pipenv shell` then run the following commands. 

For AWS 

```bash
python cloud-fw-ip-updater.py --cloud aws --fw_id sg-abc123456
```
For Digital Ocean

```bash
python cloud-fw-ip-updater.py --cloud do --fw_id AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE --do_token XXXXXXXXXXXXXXXXXXXXX
```

For Hetzner Cloud

With command line parameters:
```bash
python cloud-fw-ip-updater.py --cloud hcloud --fw_id 123456 --hcloud_token XXXXXXXXXXXXXXXXXXXXX
```

With 1Password (recommended):
```bash
python cloud-fw-ip-updater.py --cloud hcloud
```

To display your current ip:

```bash
python cloud-fw-ip-updater.py --ip_check
```
## ToDo 

* add in more error handling especially around checking jsonip.com
* Change response to be a friendly message vs. json output 
* Add in functionality to be able to pass in which AWSCLI profile you want to use

## Details

I frequently use a VPN, especially when on public or unknown Wi-Fi. For this reason I was always having to login to various Cloud providers console to update the cloud firewall updating my IP. I decided to write this python script to solve this problem.


