import boto3
import getopt
import os
import requests
from pydo import Client
from hcloud import Client as HetznerClient
import sys
import json

# boto3    - The AWS SDK for Python
# getopt   - Helps scripts to parse the args in sys.argv
# os       - Use to interact with 1password cli (op)
# requests - Python HTTP for Humans
# pydo     - Python Digital Ocean API library
# sys      - Provides access to variables used by the interpreter
# json     - JSON encoder and decoder

# Create a function to get current PUBLIC IP, returns correctly formated CIDR
def get_current_ip():
    """Returns your current IP in correct CIDR format for AWS"""
    r = requests.get(r'https://icanhazip.com')
    ip = r.text
    # strip newline from return and append /32
    result = ip.strip() + '/32' 
    return result

def show_current_ip(current_ip):
    print(f"Your current IP is {current_ip}")
    return

def aws_add_ip(current_ip, fw_id, port, protocol):
    """Add current IP to the security group"""

    # setup client for ec2
    client = boto3.client("ec2")

    # execute security group ingress Boto3 commands
    # TODO: Add in try for graceful error handling
    response = client.authorize_security_group_ingress(
        GroupId=fw_id,
        IpProtocol=protocol,
        FromPort=port,
        ToPort=port,
        CidrIp=current_ip
    )
    print(response)

def aws_remove_ip(current_ip, fw_id, port, protocol):
    """remove current IP from the security group"""

    # setup client for ec2oh haha looks like I added a remove flag on the aws side of my code, but only works if you are still o
    client = boto3.client("ec2")

    # execute security group revoke ingress Boto3 commands
    response = client.revoke_security_group_ingress(
        GroupId=fw_id,
        IpProtocol=protocol,
        FromPort=port,
        ToPort=port,
        CidrIp=current_ip
    )
    print(response)

def fetch_from_1password(item_name, success_message, error_message):
    """Fetch an item from 1Password using the CLI."""
    try:
        item = os.popen(f'op read "op://Private/{item_name}"').read().strip()
        if item:
            print(success_message)
            return item
        else:
            print(error_message)
            return None
    except Exception as e:
        print("Error:", e)
        return None

def get_do_token_from_1password():
    """Try to retrieve DigitalOcean token from 1password cli."""
    return fetch_from_1password(
        "DigitalOcean API Token/token",
        "DigitalOcean API token retrieved successfully.",
        "Error: Failed to retrieve DigitalOcean API token from 1Password."
    )

def get_do_fw_rule_from_1password():
    """Try to retrieve DigitalOcean firewall rule from 1password cli."""
    return fetch_from_1password(
        "DigitalOcean API Token/firewallrule",
        "DigitalOcean firewall rule retrieved successfully.",
        "Error: Failed to retrieve DigitalOcean firewall rule from 1Password."
    )

def digital_ocean_fw_modify(current_ip, fw_id, port, protocol, do_token, remove):
    """ Add current IP to Digital Ocean Firewall ingress """

    do_token = get_do_token_from_1password()

    if not do_token:
        print("Please set your DigitalOcean API token in 1Password under the item 'DigitalOcean API Token'.")
        sys.exit(1)

    fw_id = get_do_fw_rule_from_1password()

    if not fw_id:
        print("Please set your DigitalOcean firewall rule id in 1Password under the item 'DigitalOcean API Token'.")
        sys.exit(1)

    do_fw_rules_url = f"https://api.digitalocean.com/v2/firewalls/{fw_id}/rules"

    new_ingress = {"inbound_rules":[{"ports":"all","protocol":"tcp","sources":{"addresses":[current_ip]}},{"ports": "all","protocol":"udp","sources": {"addresses": [current_ip]}}]}

    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer Token}",
        }
    # add the correct Digital Ocean token to the header
    headers['Authorization']="Bearer " + do_token

    if remove == False:
        r = requests.post(do_fw_rules_url, headers=headers, data=json.dumps(new_ingress))
        print(f"Your current IP of {current_ip} has been added to Digital Ocean Firewall {fw_id}.")
    elif remove == True:
        r = requests.delete(do_fw_rules_url, headers=headers, data=json.dumps(new_ingress))
        print(f"Your current IP of {current_ip} has been removed from Digital Ocean Firewall {fw_id}.")

def get_hetzner_token_from_1password():
    """Try to retrieve Hetzner Cloud token from 1password cli."""
    return fetch_from_1password(
        "Hetzner/api-token",
        "Hetzner Cloud API token retrieved successfully.",
        "Error: Failed to retrieve Hetzner Cloud API token from 1Password."
    )

def get_hetzner_fw_rule_from_1password():
    """Try to retrieve Hetzner Cloud firewall rule from 1password cli."""
    return fetch_from_1password(
        "Hetzner/firewall-id",
        "Hetzner Cloud firewall rule retrieved successfully.",
        "Error: Failed to retrieve Hetzner Cloud firewall rule from 1Password."
    )

def hetzner_fw_modify(current_ip, fw_id, port, protocol, hcloud_token, remove):
    """Add or remove current IP to/from Hetzner Cloud Firewall"""
    
    if not hcloud_token:
        hcloud_token = get_hetzner_token_from_1password()
        if not hcloud_token:
            print("Please provide a Hetzner Cloud API token via --hcloud_token or set it in 1Password.")
            sys.exit(1)

    if not fw_id:
        fw_id = get_hetzner_fw_rule_from_1password()
        if not fw_id:
            print("Please provide a Hetzner Cloud firewall ID via --fw_id or set it in 1Password.")
            sys.exit(1)

    client = HetznerClient(token=hcloud_token)

    try:
        firewall = client.firewalls.get_by_id(fw_id)
        if not firewall:
            print(f"Firewall with ID {fw_id} not found.")
            sys.exit(1)

        # Get existing rules
        rules = firewall.rules

        # Create new rule for the current IP
        new_rule = {
            "direction": "in",
            "protocol": protocol,
            "source_ips": [current_ip],
            "port": str(port) if port else "any"
        }

        if remove:
            # Remove rules matching the IP
            updated_rules = [rule for rule in rules if current_ip not in rule.source_ips]
            if len(updated_rules) == len(rules):
                print(f"IP {current_ip} not found in firewall rules.")
                return
            firewall.update(rules=updated_rules)
            print(f"Your current IP of {current_ip} has been removed from Hetzner Cloud Firewall {fw_id}.")
        else:
            # Add new rule
            rules.append(new_rule)
            firewall.update(rules=rules)
            print(f"Your current IP of {current_ip} has been added to Hetzner Cloud Firewall {fw_id}.")

    except Exception as e:
        print(f"Error updating Hetzner Cloud firewall: {str(e)}")
        sys.exit(1)

# Define the usage of the app
def usage():
    """Prints usage information"""
    print()
    print( "Cloud Firewall IP Updater")
    print( "Currently for AWS, Digital Ocean & Hetzner Cloud")
    print()
    print( "Usage:")
    print( "-h --help                 - this message")
    print( "-c --cloud                - which cloud provider (aws,do,hcloud)")
    print( "-d --do_token             - Digital Ocean token for auth")
    print( "--hcloud_token            - Hetzner Cloud token for auth (optional if using 1Password)")
    print( "-f --fw_id                - id of the security group/firewall (optional for Hetzner Cloud if using 1Password)")
    print( "-i --ip_check             - show your current ip")
    print( "-p --port                 - port for rule* ONLY FOR AWS AND HETZNER CURRENTLY")
    print( "-t --protocol             - networking protcol for the rule* ONLY FOR AWS AND HETZNER CURRENTLY")
    print( "-r --remove               - remove current IP from security group")
    print()
    print( "Examples:")
    print( "cloud-fw-ip-updater.py --ip_check")
    print( "cloud-fw-ip-updater.py --cloud aws --fw_id sg-abc123456")
    print( "cloud-fw-ip-updater.py --cloud aws --fw_id sg-abc123456 --port 22 --protocol tcp")
    print( "cloud-fw-ip-updater.py --cloud do --fw_id AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEEE --do_token XXXXXXXXXXXXXXXXXXXXX")
    print( "cloud-fw-ip-updater.py --cloud do  # Uses credentials from 1Password")
    print( "cloud-fw-ip-updater.py --cloud hcloud --fw_id 123456 --hcloud_token XXXXXXXXXXXXXXXXXXXXX")
    print( "cloud-fw-ip-updater.py --cloud hcloud  # Uses credentials from 1Password")
    print()
    print( "NOTE: This requires the AWSCLI be configured OR a valid Digital Ocean/Hetzner Cloud access token, OR 1password CLI installed.")
    print()
    sys.exit(0)

def main():
    # define global variables
    fw_id = ""
    port = 22 # setting default port as 22
    protocol = "tcp" # setting tcp as default protocol
    remove = False
    cloud = "" # Which cloud provider to change firewall for aws, do, hcloud
    do_token = ""
    hcloud_token = ""
    ip_check = False

    if not len(sys.argv[1:]):
        usage()

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hfc:d:p:t:r,i",
                                   ["help", "fw_id=", "cloud=", "do_token=", "hcloud_token=", "port=", "protocol=","remove","ip_check"])
    except getopt.GetoptError as err:
        # print error and help information
        print(str(err)) # will print something like "option -q not recognized"
        usage()

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-c", "--cloud"):
            cloud = a
        elif o in ("-d", "--do_token"):
            do_token = a
        elif o in ("--hcloud_token"):
            hcloud_token = a
        elif o in ("-f", "--fw_id"):
            fw_id = a
        elif o in ("-i", "--ip_check"):
            ip_check = True
        elif o in ("-p", "--port"):
            port = int(a)
        elif o in ("-t", "--protocol"):
            protocol = a
        elif o in ("-r", "--remove"):
            remove = True
        else:
            assert False, "Unhandled Option"

    # get current public ip
    ip = get_current_ip()

    # check if ip check was passed as arg if so show ip and exit. 
    if ip_check == True:
        show_current_ip(ip)
        sys.exit()

    # expanded into multiple clouds checking for which one
    # before moving on 
    if cloud in ("aws", "AWS"):
        # add or remove current ip to the security group
        if remove == True:
            aws_remove_ip(ip, fw_id, port, protocol)
        else:
            aws_add_ip(ip, fw_id, port, protocol)
    elif cloud in ("do","DO"):
        digital_ocean_fw_modify(ip, fw_id, port, protocol, do_token, remove)
    elif cloud in ("hcloud", "HCLOUD"):
        hetzner_fw_modify(ip, fw_id, port, protocol, hcloud_token, remove)
    else:
        print("missing valid cloud provider, please use (aws/do/hcloud)")

if __name__ == "__main__":
    main()
